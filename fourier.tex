\documentclass{article}
\usepackage[margin=1in]{geometry}

\usepackage{mat357}

\usepackage{hyperref}
\hypersetup{
  colorlinks,
  linkcolor={red!50!black},
  citecolor={blue!50!black},
  urlcolor={blue!80!black}
}

\title{Fourier Notes}
\author{Jad Elkhaleq Ghalayini}

\begin{document}

\maketitle

In this exposition of the Fourier transform, we will for the most part follow Chapter 5.1 of Stein. That said, rather than using ``functions of moderate decrease," we will use Lebesgue theory. These notes are based off handwritten notes by Professor Fabio Pusateri, made available on the course page for MAT357H1S in Winter 2019-2020 at the University of Toronto. While I have put forth my best efforts, they may be incorrect and/or incomplete, so please use them with caution.

\tableofcontents

\newpage

\section{Review of Integration}

\subsection{The Integral}

\begin{definition}[Lebesgue integral]
Recall that for \(f: \reals^d \to [0, \infty)\) we define the \textbf{Lebesgue integral} by
\begin{equation}
  \int f = m(\mc{U}f)
\end{equation}
where \(\mc{U}f\) denotes the \textbf{undergraph} of \(f\)
\begin{equation}
  \mc{U}f = \{(x, y) \in \reals^d \times [0, \infty) : 0 \leq y < f(x)\}
\end{equation}
and \(m\) denotes \(d + 1\) dimensional planar Lebesgue measure.
Note that we allow \(\int f = \infty\).
For \(f: X \subset \reals^d \to [0, \infty)\), we define the integral over the set \(X\) to be
\begin{equation}
  \int_Xf = m(\mc{U}f) = \int \chi_X \circ f(x)
\end{equation}
where \(\chi_X \circ f(x)\) is extended to \(\reals^d\) by setting it to zero outside \(X\), and in this case
\begin{equation}
  \mc{U}f = \{(x, y) \in X \times [0, \infty) : 0 \leq y < f(x)\}
\end{equation}
\end{definition}

\begin{definition}[Integrability]
We say that \(f: \reals^d \to [0, \infty)\) is \textbf{integrable} (or, sometimes, \textbf{summable}) if \(\int f < \infty\)
\end{definition}

In the case of \(f: \reals^d \to \reals\), i.e. a function which is not necessarily non-negative, we can write
\begin{equation}
  f = f_+ - f_-,
  \qquad f_+(x) = \left\{\begin{array}{cl}
    f(x) & \text{if} \ f(x) \geq 0 \\
    0 & \text{otherwise}
  \end{array}\right.,
  \qquad f_-(x) = \left\{\begin{array}{cl}
    -f(x) & \text{if} \ f(x) \leq 0 \\
    0 & \text{otherwise}
  \end{array}\right.,
\end{equation}
From this, we can naturally define integrability as follows
\begin{definition}[Integral]
For \(f: \reals^d \to \reals\), we say \(f\) is \textbf{integrable} if \(f_+, f_-\) (which are non-negative) are integrable.
Noting that \(|f| = f_+ + f_-\), this is equivalent to requiring that \(|f|\) is integrable.
In this case, we define
\begin{equation}
  \int f = \int f_+ - \int f_-
\end{equation}
In the case of \(f: \reals^d \to \mbb{C}\), we say \(f\) is integrable if \(|f|\) is integrable, or equivalently \(\Re f\), \(\Im f\) are. In this case, we define
\begin{equation}
  \int f = \int\Re f + i\int \Im f
\end{equation}
In both these cases, the integral over a set is defined in the natural way.
\end{definition}

\begin{definition}[Lebesgue space]
The space of integrable functions \(f: \reals^d \to X\), where \(X = \reals\) or \(\mbb{C}\), is called \(L^1(\reals^d; \reals)\). The target and domain may be omitted if clear from the context or irrelevant.
\end{definition}

\begin{definition}
We define the \textbf{\(L^1\) norm} \((||f||_{L^1} = \int|f|\)
\label{def:l1norm}
\end{definition}

\begin{claim}
\(L^1(\reals^d)\) is a normed vector space under the \(L^1\) norm.
\end{claim}

\begin{proof}
Check
\end{proof}

\begin{claim}
\(L^1\) is complete, and is hence a \textbf{Banach space}
\end{claim}

\begin{proof}
Check
\end{proof}

A \textbf{Hilbert space} is a Banach space where the norm comes from an inner product in the sense that
\begin{equation}
  ||f||^2 = \ip{f}{f}
\end{equation}
\(L^1\) is not a Hilbert space because the norm from Definition \ref{def:l1norm} violates the parallelogram identity.

\subsection{Almost Everywhere}

Our mindset should be that functions are defined almost everywhere (a.e.). In the same way that Lebesgue measure is not affected by zero sets, changing \(f\) on a zero set does not change it (we already know such a change is irrelevant for the integral). Technically, then, we should think of \(f\) as an equivalence class of all functions \(g\) such that \(g =^0 f\) almost everywhere.

For example, consider
\begin{equation}
  \chi_{\rationals \cap [0, 1]} = \left\{\begin{array}{cl}
    1 & \text{if} \ x \in \rationals \cap [0, 1] \\ 0 & \text{otherwise}
  \end{array}\right.
\end{equation}
It's not Riemann integrable, but it \textit{is} Lebesgue integrable, and actually is equal to zero almost everywhere. In other words, ``it is" the function \(0\).

\subsection{Properties of the Integral}

\begin{proposition}[Linearity]
Let \(f, g \in L^1(\reals)\). Then
\begin{equation}
  \forall a, b \in \mbb{C}, \int af + bg = a\int f + b\int g
\end{equation}
\end{proposition}
\begin{proof}
Already done in Section 6 of the textbook
\end{proof}

\begin{proposition}[Translation Invariance]
Let \(f \in L^1(\reals)\). Then
\begin{equation}
  \forall h \in \reals, \int f(x - h)dx = \int f
\end{equation}
\end{proposition}
\begin{proof}
Given \(h \in \reals\), we define the affine transformation \(T_h: \reals^2 \to \reals^2\) by \((x, y) \mapsto (x + h, y)\). Noting that \(\det T_h = 1\) (taken as a linear transformation), we have
\begin{equation}
  \int f(x - h)dx = m(\mc{U}(x \mapsto f(x - h))) = m(T_h\mc{U}f) = \det T_h \cdot m(\mc{U}f) = m(\mc{U}f) = \int f
\end{equation}
as desired.
\end{proof}

\begin{proposition}[Dilation]
Let \(f \in L^1(\reals)\). Then
\begin{equation}
\forall \delta > 0, \int f(\delta x)dx = \frac{1}{\delta}\int f
\end{equation}
\end{proposition}
\begin{proof}
Check
\end{proof}

\begin{proposition}[Continuity of Translation]
Let \(f \in L^1(\reals)\). Then
\begin{equation}
  \lim_{h \to 0}\int|f(x - h) - f(x)|dx = 0
\end{equation}
\end{proposition}
\begin{proof}
Check
\end{proof}

\section{Introduction to the Fourier Transform}

To introduce the Fourier transform, we will mostly follow Chapter 5.1 in Stein's Fourier Analaysis. We will, however, take a different approach in that we will rely on Lebesgue theory, making use of the space \(L^1\), the dominated convergence theory, etc.

\subsection{Fourier Series}

We begin by taking a brief look a Fourier series: a theory for functions on the circle \(S^1\), or equivalently periodic functions on a closed interval (say \([0,1]\)). Fourier series, given a ``nice" \(f\), associate to it a sequence of (complex) numbers \((a_m)_{m \in \ints}\) defined by
\begin{equation}
  a_m = \int_0^1f(x)e^{-2\pi imx}dx
  \label{eq:fourier_coeff}
\end{equation}
These are called \textbf{Fourier coefficients}. Given \((a_m)\), we can, under suitable assumptions and in a suitable sense, reconstruct \(f\) by writing
\begin{equation}
  f(x) = \sum_{m \in \ints}a_me^{2\pi imx}
\end{equation}

\subsection{The Fourier Transform}

While the Fourier series applies to functions on the circle, the Fourier transform is a theory for functions over \(\reals^d\), and can be thought of as a ``continuous version of Fourier series." Similarly to Fourier series, given a ``nice" function \(f: \reals \to \reals\) (or \(\mbb{C}\)), we associate to it another function
\begin{equation}
  \hat f(\zeta) = \int_{-\infty}^\infty f(x)e^{-2\pi ix\zeta}dx
\end{equation}
Note that, in analogy to Equation \ref{eq:fourier_coeff} for Fourier coefficients, we have a continuous variable \(\zeta \in \reals\) replacing a discrete variable \(m \in \ints\). Given \(\hat f: \reals \to \mbb{C}\), we will show, again under suitable assumptions on \(f\) and in a suitable sense, that we can reconstruct \(f\) by
\begin{equation}
  f(x) = \int_{-\infty}^{\infty}\hat f(\zeta)e^{2\pi ix\zeta}d\zeta
  \label{eq:fourier_inversion}
\end{equation}
Equation \ref{eq:fourier_inversion} is called the \textbf{Fourier inversion formula}.

\begin{definition}[Fourier Transform]
For \(f \in L^1(\reals; \mbb{C})\) (we will from now on omit the \(\mbb{C}\)) define the \textbf{Fourier transform} \(\hat f: \reals \to \mbb{C}\) of \(f\), also defined \(\four{f}\), to be the function given by
\begin{equation}
  \forall \zeta \in \reals, \hat f(\zeta) = \int_{-\infty}^\infty f(x)e^{-2\pi ix\zeta}dx
\end{equation}
\end{definition}

We note that the integral above makes sense because
\begin{equation}
  |f(x)e^{-2\pi ix\zeta}| = |f(x)|\cancel{|e^{-2\pi ix\zeta}|}
  \implies \int|f(x)e^{-2\pi ix\zeta}| = \int|f| < \infty
  \implies (x \mapsto f(x)e^{-2\pi ix\zeta}) \in L^1(\reals)
\end{equation}

\begin{lemma}[Riemann-Lebesgue]
For \(f \in L^1(\reals)\), we have \(\hat f \in \mc{C}_0(\reals)\) (the set of functions which are continuous and tend to zero as \(|\zeta|\) tends to \(\infty\)).
\end{lemma}
\begin{proof}
Check
\end{proof}

We remark that if \(f \in L^1\), then \(\hat f \in \mc{C}_0\), but we \textit{cannot} necessarily say that \(\hat f \in L^1\). This is a problem because we would like to look at Equation \ref{eq:fourier_inversion} and say that it is \(f\).

\subsection{Schwartz Space}

We want to find a good enough space such that if \(f\) is in it then \(\hat f\) must be too.
\begin{definition}[Schwartz Space]
We define the Schwartz space \(\mc{S}(\reals)\) to be the set of all \(\mc{C}^\infty\) (infinitely differentiable) functions over \(\reals\) that decay rapidly as \(|x| \to \infty\), i.e.
\begin{equation}
  \forall \ell, k \geq 0, \sup_{x \in \reals}|x|^k\left|\frac{d^\ell}{dx^\ell}f(x)\right| < \infty
  \label{eq:rapid_decay}
\end{equation}
For brevity, we will write \(\mc{S} = \mc{S}\).
\end{definition}
For example, we have
\begin{claim}
The \textbf{Gaussian} \(f(x) = e^{-a^2(x - \mu)^2}\) is in \(\mc{S}\)
\end{claim}
\begin{proof}
Check
\end{proof}
We also have that smooth \(\mc{C}^\infty\) bump functions with compact support are in \(\mc{S}\). Functions like \(e^{-|x|}\), however, are not Schwartz since they are not differentiable at zero, even though they do satisfy the rapid decay property given in Equation \ref{eq:rapid_decay}

We now wish to show that the Fourier transform of a Schwartz function is a Schwartz function. We remark that Schwartz functions are dense (with respect to \(L_1\) distance in many functional spaces of interest, such as, for example, \(L^1\), and in fact even \(\mc{C}^\infty_0\).

The proof of this is based off convlution with ``good kernels" and continuity of translation. We have seen an example of these in the proof of the Weierstrass approximation theorem.

\begin{proposition}
Let \(f \in \mc{S}\), and let \(g(x) = f(x + h)\) for some \(h \in \reals\). Then
\begin{equation}
  \hat g(\zeta) = e^{2\pi i h\zeta}\hat f(\zeta)
\end{equation}
\label{prop:fourier_translation}
\end{proposition}
\begin{proof}
Check
\end{proof}

\begin{proposition}
Let \(f \in \mc{S}\), and let \(g(x) = f(x)e^{-2\pi ixh}\) for some \(h \in \reals\). Then
\begin{equation}
  \hat g(\zeta) = \hat f(\zeta + h)
\end{equation}
\label{prop:fourier_mult}
\end{proposition}
\begin{proof}
Check
\end{proof}

\begin{proposition}
Let \(f \in \mc{S}\), and let \(g(x) = f(\delta x)\) for some \(\delta \in \reals^+\). Then
\begin{equation}
  \hat g(\zeta) = \delta^{-1}\hat f(\delta^{-1}\zeta)
\end{equation}
\end{proposition}
\begin{proof}
Check
\end{proof}

\begin{proposition}
Let \(f \in \mc{S}\), and let \(g(x) = f'(x)\). Then
\begin{equation}
\hat g(\zeta) = 2\pi i\zeta \hat f(\zeta)
\end{equation}
\label{prop:fourier_diff}
\end{proposition}
\begin{proof}
Check
\end{proof}

\begin{proposition}
Let \(f \in \mc{S}\), and let \(g(x) = -2\pi ixf(x)\). Then
\begin{equation}
\hat g(\zeta) = \frac{d}{d\zeta}\hat f(\zeta)
\end{equation}
\label{prop:fourier_inv}
\end{proposition}
\begin{proof}
Check
\end{proof}

Note that Propositions \ref{prop:fourier_translation} and \ref{prop:fourier_mult} together roughly state that the Fourier transform exchanges translation and modulation (multiplication by \(e^{i\theta}\) for \(\theta \in \reals\)).
Similarly, Propositions \ref{prop:fourier_diff} and \ref{prop:fourier_inv} together roughly say that the Fourier transform exchanges differentiation and multiplication by the position variable and a constant.

\begin{theorem}
If \(f \in \mc{S}\) then \(\hat f \in \mc{S}\)
\end{theorem}
\begin{proof}
Check
\end{proof}

\begin{lemma}
Let \(f(x) = e^{-\pi x^2}\) denote the Gaussian function. Then
\begin{equation}
  \int f = 1, \qquad \hat f = f
\end{equation}
\end{lemma}
\begin{proof}
Check
\end{proof}

By dilation, we have that
\begin{equation}
\mc{F}(e^{-\pi x^2})(\zeta) = e^{-\pi\zeta^2}, \qquad K_\delta(x) = \frac{1}{\delta}e^{-\pi x^2/\delta} \implies \hat{K_\delta}(\zeta) = e^{-\pi\delta\zeta^2}
\end{equation}
We note that as \(\delta \to 0\), \(K_\delta\) peaks (concentrates) around \(x = 0\) while \(\hat{K_\delta}\) spreads out. This is a manifestation of the \textbf{Heisenberg uncertainty principle}.

\subsection{Fourier Inversion}

\begin{definition}
A family of smooth functions \(\{K_\delta\}\) for \(\delta > 0\) such that
\begin{enumerate}[label=(\roman*)]

  \item \(\int K_\delta = 1\)

  \item There exists some \(C > 0\) such that \(\forall \delta \in \reals^+, \int |K_\delta| \leq C\)

  \item For any \(\eta > 0\), \(\lim_{\delta \to 0}\int_{\reals\setminus[-\eta, \eta]}|K_\delta| = 0\)

\end{enumerate}
is called a \textbf{family of good kernels}
\end{definition}

\begin{claim}
\(K_\delta(x) = \frac{1}{\sqrt{\delta}}e^{-\pi x^2/\delta}\) is a family of good kernels
\end{claim}
\begin{proof}
Check.
\end{proof}

\begin{lemma}
If \(f \in \mc{S}\) and \(\{K_\delta\}\) is a family of good kernels then \(f * K_\delta\) converges uniformly to \(f\) as \(\delta \to 0\)
\end{lemma}
\begin{proof}
We note that this proof is the same as in the Weierstrass approximation theorem.

Check.
\end{proof}

We remark that with a similar argument we can show that if \(f \in L^1\) then
\begin{equation}
  \lim_{\delta \to 0}K_\delta * f = f
\end{equation}
in \(L^1\), using continuity of translation.

\begin{proposition}
If \(f, g \in \mc{S}\) then
\begin{equation}
  \int f\hat g = \int \hat fg
\end{equation}
\end{proposition}
\begin{proof}
Check
\end{proof}

\begin{theorem}[Fourier Inversion]
If \(f \in \mc{S}\), then
\begin{equation}
  f(x) = \int e^{2\pi ix\zeta}\hat f(\zeta)d\zeta
\end{equation}
\label{thm:fourier_inv}
\end{theorem}
\begin{proof}
Check.
\end{proof}

We remark that the Fourier transform is its own inverse up to changing \(x \mapsto -x\). Namely, taking \(\mc{F}: \mc{S} \to \mc{S}\), we define the adjoint \(\mc{F}^*: \mc{S} \to \mc{S}\) by
\begin{equation}
  \mc{F}^*f(\zeta) = \int e^{2\pi ix\zeta}f(x)dx
\end{equation}
We showed in Theorem \ref{thm:fourier_inv} that \(\mc{F}^* \circ \mc{F} = \Id\) on \(\mc{S}\). Hence,
\begin{equation}
  (\mc{F}f)(\zeta) = (\mc{F}^*f)(-\zeta) \implies \mc{F} \circ \mc{F}^* = \Id: \mc{S} \to \mc{S} \implies \mc{F}^* = \mc{F}^{-1}
\end{equation}
and \(\mc{F}\) is a bijection on \(\mc{S}\).

\subsection{Plancharel's Theorem}

Recall that for \(f, g \in L^1\), we define their \textbf{convolution} \(f * g \in L^1\) to be given by
\begin{equation}
  (f * g)(x) = \int f(t)g(x - t)dt
\end{equation}

\begin{proposition}[Closure]
If \(f, g \in \mc{S}\), then \(f * g \in \mc{S}\)
\end{proposition}
\begin{proof}
Check
\end{proof}

\begin{proposition}[Commutativity]
If \(f, g \in \mc{S}(\reals\), then \(f * g = g * s \in \mc{S}\)
\end{proposition}
\begin{proof}
Check
\end{proof}

\begin{proposition}[Convolution Theorem]
If \(f, g \in \mc{S}\), then \(\four{f \cdot g} = \four{f} * \four{g}\), \(\four{f * g} = \four{f} \cdot \four{g}\).
\end{proposition}
\begin{proof}
Check
\end{proof}

We equip \(\mc{S}\) with an inner product
\begin{equation}
  \forall f, g \in \mc{S}, \ip{f}{g} = \int f(x)\ol{g}(x)dx
\end{equation}
This induces an associated norm
\begin{equation}
  \forall f \in \mc{S}, ||f||^2 = \ip{f}{f} = \int f\ol{f} = \int|f|^2
\end{equation}
usually called the \textbf{\(L^2\)-norm}

\begin{theorem}[Plancharel's Theorem]
  If \(f \in \mc{S}\), then \(||\hat{f}||_{L^2} = ||f||_{L^2}\)
  \label{thm:plancharel}
\end{theorem}
\begin{proof}
Check.
\end{proof}

\begin{proposition}
\(\mc{S}\) is dense in \(L^2\), the set of integrable functions \(f\) with \(\int|f|^2 < \infty\)
\end{proposition}
\begin{proof}
Check.
\end{proof}

This fact allows us to define the Fourier transform for \(f \in L^2\): take a sequence \(\psi_m \in S\) such that \(\lim_{m \to \infty}\psi_m = f\) in \(L^2\). \(\psi_m\) is Cauchy in \(L^2\), implying that \(\hat \psi_m\) is Cauchy in \(L^2\)by Plancharels' theorem (Theorem \ref{thm:plancharel}). Hence, by the completeness of \(L^2\), it follows that \(\hat\psi_m\) converges in \(L^2\).

\begin{definition}[Fourier transform on \(L^2\)]
We define, for \(f \in L^2\), the Fourier transform
\begin{equation}
  \mc{F}_2(f) = \lim_{m \in \infty}\hat\psi_m
\end{equation}
where the \(\psi_m \in \mc{S}\) converge to \(f\) in \(L^2\) and the limit is taken in \(L^2\)
\end{definition}

\begin{claim}
\(\mc{F}_2\) as defined above is well-defined and coincides with \(\mc{F}\)
\end{claim}
\begin{proof}
Check.
\end{proof}


\end{document}
